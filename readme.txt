My Zork Game Instructions

	1. Plugins:
	    Shade Plugin for building jar file

	2. Dependencies:
	    junit version 4.12 for unit testing in source doe

	3. Compilation:
            mvn clean install

	4. Run:
	    java -jar zorkGame-1.0-SNAPSHOT.jar

	5. Done: 
            Enjoy the text-based Zork Game!