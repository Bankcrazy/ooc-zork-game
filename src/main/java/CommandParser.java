import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 * Created by Bank on 1/30/17.
 */
class CommandParser {
    private Command commands;

    CommandParser() {commands = new Command();}

    Command getCommand(){
        String inputLine = "";
        String firstWord;
        String secondWord;

        System.out.print("> ");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            inputLine = reader.readLine();
        }
        catch(java.io.IOException exc) {
            System.out.println ("There was an error during reading: " + exc.getMessage());
        }

        StringTokenizer tokenizer = new StringTokenizer(inputLine);

        if(tokenizer.hasMoreTokens())
            firstWord = tokenizer.nextToken();
        else
            firstWord = null;
        if(tokenizer.hasMoreTokens())
            secondWord = tokenizer.nextToken();
        else
            secondWord = null;

        if(commands.isCommand(firstWord))
            return new Command(firstWord, secondWord);
        else
            return new Command(null, secondWord);
    }

    void showCommands() {commands.showAll();}
}
