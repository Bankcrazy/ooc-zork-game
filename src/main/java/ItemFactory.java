import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bank on 1/30/17.
 */
class ItemFactory {
    private List<Item> itemsList = new ArrayList<>();
    // weapon item type

    // melee sub type
    Item baseballClub = new Item("Baseball-Club", "Available","Weapon","Melee" ,10, 3);
    Item knife = new Item("Knife","Available","Weapon","Melee",20,2);

    // range sub type
    Item pistol = new Item("Pistol","Available","Weapon","Range",30,2);

    // consumable item type
    Item bandage = new Item("Bandage", "Available", "Consumable","",5,3);
    Item medKit = new Item("Medical-Kit", "Available", "Consumable","",25,2);

    // objective item type
    Item key = new Item("Key", "Available", "Objective","",0,1);

    Item makeItem (String itemName){
        for (Item item: itemsList){
            if (item.shortDescription().equals(itemName)){
                return new Item(item.shortDescription(), item.getStatus(), item.getType(), item.getSubType(), item.getDamage(), item.getDurability());
            }
        }
        return null;
    }

    void addItem(Item item) {
        itemsList.add(item);
    }


}
