import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bank on 1/30/17.
 */
class MonsterFactory {
    private List<Monster> monstersList = new ArrayList<>();
    Monster smallZombie = new Monster("Small Zombie", "Alive",10, 5);
    Monster crawlerZombie = new Monster("Crawler Zombie", "Alive",20, 10);
    Monster tankZombie = new Monster("Tank Zombie", "Alive",40, 20);
    Monster superZombie = new Monster("Super Zombie", "Alive",90, 30);

    Monster makeMonster (String monsterName){
        for (Monster monster: monstersList){
            if (monster.shortDescription().equals(monsterName)){
                return new Monster(monster.shortDescription(), monster.getStatus(), monster.getHealth() ,monster.getHitPoint());
            }
        }
        return null;
    }

    void addMonster(Monster monster) {
        monstersList.add(monster);
    }
}
