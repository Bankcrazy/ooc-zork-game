import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bank on 1/30/17.
 */
class Player {
    private final static int MAX_ITEMS = 3;
    private final static int MAX_HP = 100;
    private int health;
    private List<Item> inventory = new ArrayList<>();

    Player(int health) {this.health = health;}

    int getHealth() {return health;}
    List<Item> getInventory(){return inventory;}
    String getItemsName(){return itemString();}
    int getItemsCount(){return inventory.size();}

    int editHealth(int amount){
        int newHealth = health + amount;
        if (newHealth > MAX_HP) {
            int healAmount = MAX_HP -health;
            this.health = health + healAmount;
            return healAmount;
        } else {
            this.health = newHealth;
            return amount;
        }

    }

    boolean addItem(Item item){
        if (inventory.size() < MAX_ITEMS) {
            inventory.add(item);
            return true;
        } else {
            return false;
        }
    }

    private String itemString()
    {
        String returnString = "";
        for(Item item : inventory) {
            returnString += item.shortDescription() + " ";
        }
        return returnString;
    }

}
