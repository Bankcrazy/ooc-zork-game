/**
 * Created by Bank on 1/30/17.
 */
class Monster {
    private String description;
    private int health;
    private int hitPoint;
    private String status;

    Monster(String description, String status, int health, int hitPoint) {
        this.description = description;
        this.health = health;
        this.hitPoint = hitPoint;
        this.status = status;

    }

    String shortDescription() {return description;}
    int getHealth() {return health;}
    int getHitPoint() {return hitPoint;}
    String getStatus() {return status;}
    void editStatus(String newStatus) {this.status = newStatus;}
    void editHealth(int newHealth) {this.health = newHealth;}

}
