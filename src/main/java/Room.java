import java.util.*;

/**
 * Created by Bank on 1/30/17.
 */
class Room {
    private final static int MAX_ITEMS = 3;
    private String description;
    private HashMap exits;
    private Monster monster;
    private String objective;
    private String objectiveItem;
    private List<Item> items = new ArrayList<>();

    Room(String description)
    {
        this.description = description;
        exits = new HashMap();
    }

    void setObjective(String objective){this.objective = objective;}

    void setObjectiveItem(String item){this.objectiveItem = item;}

    void addMonster(Monster monster){this.monster=monster;}

    void setExits(Room north, Room east, Room south, Room west)
    {
        if(north != null)
            exits.put("north", north);
        if(east != null)
            exits.put("east", east);
        if(south != null)
            exits.put("south", south);
        if(west != null)
            exits.put("west", west);
    }

    boolean addItem(Item item) {
        if (items.size() < MAX_ITEMS) {
            items.add(item);
            return true;
        } else {
            return false;
        }
    }

    List<Item> getItems() {return items;}

    String getObjective(){return objective;}

    String getObjectiveItem(){return objectiveItem;}

    String shortDescription() {return description;}

    String longDescription() {return "You are in " + description + ".\n" + exitString() + "\n" + itemString() + "\n" + monsterString();}

    String itemDescription() {return itemString();}

    String exitDescription() {return exitString();}

    String monsterDescription() {return monsterString();}

    Monster getMonster(){return monster;}


    private String exitString(){
        String returnString = "Exit(s):";
        Set keys = exits.keySet();
        for (Object key : keys) returnString += " " + key;
        return returnString;
    }

    private String itemString(){
        String returnString = "Item(s):";
        for(Item item : items) {
            returnString += " " + item.shortDescription() + " (" + item.getStatus() + ")";
        }
        return returnString;
    }

    private String monsterString(){
        if (monster == null) return "Monster: ";
        else return "Monster: " + monster.shortDescription() + " (" + monster.getStatus() + ")";
    }


    Room nextRoom(String direction) {return (Room)exits.get(direction);}
}
