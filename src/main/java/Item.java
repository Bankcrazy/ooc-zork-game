/**
 * Created by Bank on 1/30/17.
 */
class Item {
    private String description;
    private int damage;
    private int durability;
    private String type;
    private String subType;
    private String status;

    Item(String description, String status, String type, String subType ,int damage, int durability) {
        this.description = description;
        this.status = status;
        this.type = type;
        this.subType = subType;
        this.damage = damage;
        this.durability = durability;
    }

    String shortDescription() {return description;}
    String getStatus() {return status;}
    String getType(){return type;}
    String getSubType(){return subType;}
    int getDurability() {return durability;}
    int getDamage() {return damage;}
    void editDurability(int amount) {this.durability = durability +amount;}
    void editStatus(String newStatus) {this.status=newStatus;}
}
