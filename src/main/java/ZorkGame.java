import java.util.Iterator;
import java.util.List;

/**
 * Created by Bank on 1/30/17.
 */
class ZorkGame {

        private CommandParser parser;
        private Player player;
        private Room currentRoom;
        int level = 1;

        ZorkGame(){
            createMap();
            parser = new CommandParser();
        }

        private void createMap(){
            // Load all the game items and monster to the map
            MonsterFactory monster = new MonsterFactory();
            ItemFactory item = new ItemFactory();

            monster.addMonster(monster.smallZombie);
            monster.addMonster(monster.crawlerZombie);
            monster.addMonster(monster.tankZombie);
            monster.addMonster(monster.superZombie);

            item.addItem(item.baseballClub);
            item.addItem(item.knife);
            item.addItem(item.bandage);
            item.addItem(item.medKit);
            item.addItem(item.pistol);
            item.addItem(item.key);

            // Level One room name
            Room outside, lobby, menToliet, womenToliet, shortHallwayA, longHallwayA, eastHallwayA,
                    endHallwayA, westHallwayA, nurseryRoom0, nurseryRoom1, houseKeeping0, staircase
                    ,levelTwoEntrance;

            // Level One
            outside = new Room("front of Health Center");
            lobby = new Room("lobby of Health Center");
            womenToliet = new Room("Women's Restroom");
            menToliet = new Room("Men's Restroom");
            shortHallwayA = new Room ("Short Hallway A");
            longHallwayA = new Room("Long Hallway A");
            endHallwayA = new Room("End of Hallway A");
            eastHallwayA = new Room("East of Hallway A");
            westHallwayA = new Room("West of Hallway A");
            nurseryRoom0 = new Room("Nursery Room No. 101");
            nurseryRoom1 = new Room("Nursery Room No. 100");
            houseKeeping0 = new Room("House-Keeping Room No. 110");
            staircase = new Room("front of the Staircase to Second floor");
            levelTwoEntrance = new Room("Entrance to second floor");


            outside.setExits(lobby, null, null, null);
            lobby.setExits(shortHallwayA, womenToliet, outside, menToliet);
            womenToliet.setExits(null,null,null,lobby);
            menToliet.setExits(null,lobby,null,null);
            shortHallwayA.setExits(longHallwayA,null,lobby,null);
            longHallwayA.setExits(endHallwayA,null,shortHallwayA,null);
            endHallwayA.setExits(null,eastHallwayA,longHallwayA,westHallwayA);
            eastHallwayA.setExits(nurseryRoom0,houseKeeping0,null,endHallwayA);
            nurseryRoom0.setExits(null,null,eastHallwayA,null);
            houseKeeping0.setExits(null,null,null,eastHallwayA);
            westHallwayA.setExits(nurseryRoom1,endHallwayA,null,staircase);
            nurseryRoom1.setExits(null,null,westHallwayA,null);
            staircase.setExits(null,westHallwayA,null,levelTwoEntrance);

            lobby.addItem(item.makeItem("Baseball-Club"));
            lobby.addMonster(monster.makeMonster("Small Zombie"));

            womenToliet.addItem(item.makeItem("Bandage"));
            womenToliet.addMonster(monster.makeMonster("Small Zombie"));

            menToliet.addItem(item.makeItem("Bandage"));

            shortHallwayA.addMonster(monster.makeMonster("Small Zombie"));

            longHallwayA.addItem(item.makeItem("Baseball-Club"));

            endHallwayA.addItem(item.makeItem("Knife"));
            endHallwayA.addItem(item.makeItem("Bandage"));

            nurseryRoom0.addItem(item.makeItem("Medical-Kit"));

            houseKeeping0.addMonster(monster.makeMonster("Crawler Zombie"));

            eastHallwayA.addMonster(monster.makeMonster("Crawler Zombie"));

            nurseryRoom1.addItem(item.makeItem("Knife"));
            nurseryRoom1.addItem(item.makeItem("Bandage"));

            staircase.addMonster(monster.makeMonster("Tank Zombie"));

            levelTwoEntrance.setObjective("Kill");

            // Level Two room name
            Room hallwayB, eastHallwayB, westHallwayB, laboratoryEntry, janitorRoom
                    , levelThreeEntrance;

            hallwayB = new Room("Hallway B");
            eastHallwayB = new Room("East of Hallway B");
            westHallwayB = new Room("West of Hallway B");
            laboratoryEntry = new Room("Entrance to Laboratory");
            janitorRoom = new Room("Janitor Room");
            levelThreeEntrance = new Room("Laboratory");

            // Level 2
            levelTwoEntrance.setExits(hallwayB,null,null,null);
            hallwayB.setExits(laboratoryEntry,eastHallwayB,null,westHallwayB);
            eastHallwayB.setExits(null,null,null,hallwayB);
            westHallwayB.setExits(null,hallwayB,null,janitorRoom);
            janitorRoom.setExits(null,westHallwayB,null,null);
            laboratoryEntry.setExits(levelThreeEntrance, null,hallwayB,null);

            hallwayB.addMonster(monster.makeMonster("Small Zombie"));
            hallwayB.addItem(item.makeItem("Knife"));

            eastHallwayB.addItem(item.makeItem("Medical-Kit"));

            westHallwayB.addItem(item.makeItem("Knife"));

            janitorRoom.addItem(item.makeItem("Key"));

            hallwayB.addMonster(monster.makeMonster("Crawler Zombie"));

            levelThreeEntrance.setObjective("Item");
            levelThreeEntrance.setObjectiveItem("Key");

            // Level Three room name
            Room labHallway, virusSampleRoom, surgeryRoom, researchRoom, operatingRoom
                    , finalRoom;

            labHallway = new Room("Lab Hallway");
            virusSampleRoom = new Room("Virus Sample Room");
            surgeryRoom = new Room("Surgery Room");
            researchRoom = new Room("Research Room");
            operatingRoom = new Room("Operating Room");
            finalRoom = new Room("The Cure Room");

            levelThreeEntrance.setExits(labHallway,null,null,null);
            virusSampleRoom.setExits(null,null,null,labHallway);
            labHallway.setExits(researchRoom,virusSampleRoom,null,surgeryRoom);
            surgeryRoom.setExits(null,labHallway,null,null);
            researchRoom.setExits(finalRoom,null,labHallway,operatingRoom);
            operatingRoom.setExits(null,researchRoom,null,null);

            levelThreeEntrance.addMonster(monster.makeMonster("Small Zombie"));
            levelThreeEntrance.addItem(item.makeItem("Knife"));

            virusSampleRoom.addItem(item.makeItem("Medical-Kit"));

            labHallway.addMonster(monster.makeMonster("Crawler"));

            surgeryRoom.addItem(item.makeItem("Knife"));
            surgeryRoom.addItem(item.makeItem("Bandage"));

            researchRoom.addMonster(monster.makeMonster("Super Zombie"));

            operatingRoom.addItem(item.makeItem("Pistol"));

            finalRoom.setObjective("Kill");

            currentRoom = outside;
        }

        void play(){
            printWelcome();
            player = new Player(100);

            boolean finished = false;
            while (! finished){
                Command command = parser.getCommand();
                finished = processCommand(command);
            }
            System.out.println("Thank you for playing.  Good bye.");
        }

        private void printWelcome(){
            System.out.println();
            System.out.println("Welcome to Zork Game!");
            System.out.println("Type 'help' if you need help.");
            System.out.println();
            System.out.println(currentRoom.longDescription());
        }

        private boolean processCommand(Command command){
            if(command.isUnknown()){
                System.out.println("Invalid Command.");
                return false;
            }

            String commandWord = command.getCommandWord().toLowerCase();
            switch (commandWord) {
                case "help":
                    printHelp();
                    break;
                case "go":
                    return goRoom(command);
                case ("take"):
                    takeItem(command);
                    break;
                case ("drop"):
                    dropItem(command);
                    break;
                case "attack-with":
                    return attack(command);
                case "use":
                    use(command);
                    break;
                case "player-info":
                    System.out.println("Your current status: ");
                    System.out.println("Health: " + player.getHealth());
                    System.out.println("Inventory: " + player.getItemsName());
                    break;
                case "map-info":
                    System.out.println("Current Map Information:");
                    System.out.println("" + currentRoom.shortDescription());
                    System.out.println("" + currentRoom.exitDescription());
                    System.out.println("" + currentRoom.itemDescription());
                    System.out.println("" + currentRoom.monsterDescription());
                    break;
                case "quit":
                    if (command.hasSecondWord())
                        System.out.println("Quit what?");
                    else
                        return true;
                    break;
            }
            return false;
        }

        private void printHelp() {
            System.out.println("Available commands are:");
            parser.showCommands();
        }

    private Boolean goRoom(Command command){
        if(!command.hasSecondWord()){
            System.out.println("go where?");
            return false;
        }

        String direction = command.getSecondWord().toLowerCase();
        Room nextRoom = currentRoom.nextRoom(direction);

        if (nextRoom == null){
            System.out.println("Invalid Direction!");
            return false;
        } else {
            String nextRoomObjective = nextRoom.getObjective();
            if (nextRoomObjective == null) {
                currentRoom = nextRoom;
                System.out.println(currentRoom.longDescription());
                return false;
            } else if (nextRoomObjective.equals("Kill")){
                if (currentRoom.getMonster().getStatus().equals("Dead")){
                    currentRoom = nextRoom;
                    level += 1;
                    if (level==4){
                        System.out.println("Congratulations, you found the cure and save man-kind from apocalypse");
                        System.out.println("You Beat the Game!!");
                        return true;
                    }else {
                        System.out.println("Congratulations, you made it to level: " + level);
                        System.out.println("Now there is no going back to the last level rooms, only moving forward");
                        System.out.println(currentRoom.longDescription());
                        return false;
                    }

                } else {
                    System.out.println("Please kill the monster in this room to proceed to '"+direction+"' exit");
                    return false;
                }
            } else {
                boolean itemCheck = false;
                String objectiveItem = nextRoom.getObjectiveItem();
                for(Item item : player.getInventory()) {
                    if (item.shortDescription().equals(objectiveItem)){
                        itemCheck = true;
                    }
                }

                Iterator<Item> iter = player.getInventory().iterator();
                while(iter.hasNext()){
                    if(iter.next().shortDescription().equals(objectiveItem))
                        iter.remove();
                }

                if (!itemCheck){
                    System.out.println("You need to find '"+objectiveItem+"' item to proceed to '"+direction +"' exit");
                    return false;
                }
                else {
                    System.out.println("You unlock the room by using '"+objectiveItem+"'");
                    level += 1;
                    currentRoom = nextRoom;
                    System.out.println("Congratulations, you made it to level: " + level);
                    System.out.println("Now there is no going back to the last level rooms, only moving forward");
                    System.out.println(currentRoom.longDescription());
                    return false;
                }
            }
        }
    }

    private void takeItem(Command command){
        if(!command.hasSecondWord()){
            System.out.println("take what item?");
            return;
        }
        String pickItem = command.getSecondWord().toLowerCase();
        List<Item> roomItems = currentRoom.getItems();

        // itemCheck contain following code (0,1,2,3)
        // code 0 = invalid item name, item not exists
        // code 1 = item available and added to player's inventory
        // code 2 = item has already been taken by player
        // code 3 = same item exists in player inventory, so item useCount is increase accordingly instead
        int itemCheck = 0;

        if (player.getItemsCount() == 3){
            System.out.println("Item inventory full, please drop item in your inventory by using 'drop {Item Name}' command");
        } else {
            Item targetItem = null;
            for(Item item : roomItems) {
                if (item.shortDescription().toLowerCase().equals(pickItem)){
                    if (item.getStatus().equals("Available")) {
                        targetItem = item;
                        item.editStatus("Taken");
                        itemCheck = 1;
                    } else itemCheck = 2;
                }
            }

            if (itemCheck==0){ System.out.println("There is no such item");}
            else if (itemCheck==1){
                for(Item item : player.getInventory()) {
                    if (item.shortDescription().toLowerCase().equals(pickItem)){
                        item.editDurability(targetItem.getDurability());
                        itemCheck = 3;
                    }
                }
                if (itemCheck==3) {
                    System.out.println("'"+ pickItem+"' already exists in your inventory");
                    System.out.println(""+targetItem.getDurability()+" Item durability has been add to '"+pickItem+"' instead");
                } else {
                    System.out.println("'"+ pickItem+"' added to inventory");
                    player.addItem(targetItem);
                }
            }
            else  System.out.println("'"+pickItem+"' already taken");
        }
    }

        private void dropItem(Command command){
            if(!command.hasSecondWord()) {
                System.out.println("drop what item?");
                return;
            }
            String dropItem = command.getSecondWord().toLowerCase();
            // itemCheck contain following code (0,1,2)
            // code 0 = invalid item name, item not exists
            // code 1 = item is of type 'Objective' which cannot be drop, becasue it is required to access certain room
            // code 2 = item is successfully remove from player inventory
            int itemCheck = 0;
            if (player.getItemsCount() == 0){
                System.out.println("Inventory is empty");
            } else {
                Iterator<Item> iter = player.getInventory().iterator();
                while(iter.hasNext()){
                    Item currentItem = iter.next();
                    if(currentItem.shortDescription().toLowerCase().equals(dropItem)){
                        if (currentItem.getType().equals("Objective")) {
                            itemCheck = 1;
                        } else {
                            iter.remove();
                            itemCheck = 2;
                        }
                    }
                }
                if (itemCheck==0){ System.out.println("There is no such item");}
                else if (itemCheck==1) System.out.println("Cannot drop '"+dropItem+"' because it is objective item");
                else System.out.println("'"+ dropItem+ "' removed from inventory");
            }
        }

    Boolean attack(Command command) {
        if(!command.hasSecondWord()){
            System.out.println("attack with what item?");
            return false;
        }

        String weapon = command.getSecondWord().toLowerCase();
        Monster roomMonster = currentRoom.getMonster();
        Boolean itemCheck = false;
        int itemDamage = 0;
        String itemSubType = null;

        if (player.getItemsCount() == 0){
            System.out.println("Inventory is empty");
        } else if (roomMonster.getStatus().equals("Dead")) System.out.println("'"+roomMonster.shortDescription()+"' is already dead");
        else {
            for(Item item : player.getInventory()) {
                if (item.shortDescription().toLowerCase().equals(weapon)){
                    if (item.getType().equals("Weapon")){
                        itemCheck = true;
                        itemDamage = item.getDamage();
                        itemSubType = item.getSubType();

                        int playerHealth = player.getHealth();
                        int monsterHealth = roomMonster.getHealth();
                        int monsterDamage = roomMonster.getHitPoint();

                        item.editDurability(-1);
                        roomMonster.editHealth(monsterHealth-itemDamage);
                        if (itemSubType.equals("Melee")){
                            player.editHealth(-monsterDamage);
                        }
                    }
                }
            }
            Iterator<Item> iter = player.getInventory().iterator();
            while(iter.hasNext()){
                if(iter.next().getDurability()==0)
                    iter.remove();
            }

            if (roomMonster.getHealth() <= 0){
                roomMonster.editStatus("Dead");
            }

            if (!itemCheck){
                System.out.println("No such weapon");
                return false;
            } else {
                System.out.println("You attack '"+roomMonster.shortDescription()+";"+"for "+itemDamage+" damage");
                if (itemSubType.equals("Melee")) {
                    System.out.println("'"+roomMonster.shortDescription()+"'"+"attack you for "+roomMonster.getHitPoint()+" damage");
                }
                if (player.getHealth() <= 0){
                    System.out.println("You got killed by '"+roomMonster.shortDescription()+"'");
                    return true;
                }
            }
        }
        return false;
    }

    private void use(Command command){
        if(!command.hasSecondWord()){
            System.out.println("use what?");
            return;
        }
        String useItem = command.getSecondWord().toLowerCase();
        // itemCheck contain following code (0,1,2)
        // code 0 = invalid item name, item not exists
        // code 1 = item are of correct type and used by player
        // code 2 = item are of incorrect type and cannot be used by player using command 'use {Item Name}'
        int itemCheck = 0;
        String itemName = null;
        int itemDamage = 0;
        int healthReceived = 0;
        if (player.getItemsCount() == 0){
            System.out.println("Inventory is empty");
        } else {
            for(Item item : player.getInventory()) {
                if (item.shortDescription().toLowerCase().equals(useItem)){
                    if (item.getType().equals("Consumable")){
                        item.editDurability(-1);
                        itemCheck = 1;
                        itemName = item.shortDescription();
                        itemDamage = item.getDamage();

                        healthReceived = player.editHealth(itemDamage);
                    } else itemCheck = 2;
                }
            }

            Iterator<Item> iter = player.getInventory().iterator();
            while(iter.hasNext()){
                if(iter.next().getDurability()==0)
                    iter.remove();
            }

            if (itemCheck==0){ System.out.println("There is no such item");}
            else if (itemCheck==1){
                System.out.println("You used "+ itemName + " and heal for " + healthReceived + " health");
                System.out.println("Your current health: " + player.getHealth());
            }
            else {
                System.out.println("Cannot use item: "+useItem+"' because of invalid item type");
            }
        }

    }
}
