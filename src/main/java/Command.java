/**
 * Created by Bank on 1/30/17.
 */
class Command {

    private String commandWord;
    private String secondWord;

    private static final String validCommands[] = {
            "player-info", "map-info" ,"take", "drop", "use", "go", "attack-with", "help", "quit"
    };

    Command(String firstWord, String secondWord) {
        commandWord = firstWord;
        this.secondWord = secondWord;
    }

    Command() {
        // make command
    }

    boolean isCommand(String aString){
        for (String validCommand : validCommands) {
            if (validCommand.equals(aString))
                return true;
        }
        return false;
    }

    void showAll(){
        System.out.println("player-info \n" +
                "map-info \n" +
                "take{item-name} \n" +
                "drop{item-name} \n" +
                "use{item-name} \n" +
                "attack-with{item-name} \n" +
                "go{direction} \n"+
                "help \n" +
                "quit");
    }

    String getCommandWord() {return commandWord;}

    String getSecondWord() {return secondWord;}

    boolean isUnknown() {return (commandWord == null);}

    boolean hasSecondWord() {return (secondWord != null);}

}
