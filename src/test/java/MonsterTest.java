import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Bank on 2/3/17.
 */
public class MonsterTest {
    private Monster testMonster = new Monster("Test Zombie", "Alive",100, 10);

    @Test
    public void shortDescriptionTest(){

        String expectedDescription = "Test Zombie";
        assertEquals(expectedDescription, testMonster.shortDescription());
    }

    @Test
    public void getHealthTest(){
        int expectedHealth = 100;
        assertEquals(expectedHealth, testMonster.getHealth());
    }

    @Test
    public void getHitPointTest(){
        int expectedHitPoint = 10;
        assertEquals(expectedHitPoint, testMonster.getHitPoint());
    }

    @Test
    public void getStatusTest(){;
        String expectedStatus = "Alive";
        assertEquals(expectedStatus, testMonster.getStatus());
    }

    @Test
    public void editStatusTest(){;
        String expectedStatus = "Dead";
        testMonster.editStatus("Dead");
        assertEquals(expectedStatus, testMonster.getStatus());
    }

    @Test
    public void editHealthTest(){;
        int expectedHealth = 0;
        testMonster.editHealth(0);
        assertEquals(expectedHealth, testMonster.getHealth());
    }
}
