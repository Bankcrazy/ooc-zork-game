import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Bank on 2/3/17.
 */
public class PlayerTest {
    private Player testPlayer = new Player(100);

    @Test
    public void getHealthTest(){
        int expectedHealth = 100;
        assertEquals(expectedHealth, testPlayer.getHealth());
    }

    @Test
    public void getInventoryTest(){
        List<Item> expectedInventory = new ArrayList<>();
        assertEquals(expectedInventory, testPlayer.getInventory());
    }

    @Test
    public void getItemsNameTest(){
        String expectedItemName = "";
        assertEquals(expectedItemName, testPlayer.getItemsName());
    }

    @Test
    public void getItemsCountTest(){
        int expectedItemsCount = 0;
        assertEquals(expectedItemsCount, testPlayer.getItemsCount());
    }

    @Test
    public void editHealthTest(){
        int expectedHealth = 50;
        testPlayer.editHealth(-50);
        assertEquals(expectedHealth, testPlayer.getHealth());
    }

    @Test
    public void addItemTest(){
        Item testItem = new Item("testItem","Available","Test","None", 0,0);
        assertTrue(testPlayer.addItem(testItem));
    }

}
