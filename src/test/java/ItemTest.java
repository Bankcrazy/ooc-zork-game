import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 * Created by Bank on 2/3/17.
 */
public class ItemTest {
    private Item testItem = new Item("testItem","Available","Test","None", 0,0);

    @Test
    public void shortDescriptionTest(){

        String expectedDescription = "testItem";
        assertEquals(expectedDescription, testItem.shortDescription());
    }

    @Test
    public void getStatusTest(){
        String expectedStatus = "Available";
        assertEquals(expectedStatus, testItem.getStatus());;
    }

    @Test
    public void getTypeTest(){
        String expectedType = "Test";
        assertEquals(expectedType, testItem.getType());
    }

    @Test
    public void getSubTypeTest(){;
        String expectedSubType = "None";
        assertEquals(expectedSubType, testItem.getSubType());
    }

    @Test
    public void getDurabilityTest(){;
        int expectedDurability = 0;
        assertEquals(expectedDurability, testItem.getDurability());
    }

    @Test
    public void getDamageTest(){;
        int expectedDamage = 0;
        assertEquals(expectedDamage, testItem.getDamage());
    }

    @Test
    public void editDurabilityTest(){
        int expectedDurability = 1;
        testItem.editDurability(1);
        assertEquals(expectedDurability, testItem.getDurability());
    }

    @Test
    public void editStatusTest(){
        String expectedStatus = "Taken";
        testItem.editStatus("Taken");
        assertEquals(expectedStatus, testItem.getStatus());
    }

}
