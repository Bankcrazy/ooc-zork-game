import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Created by Bank on 2/3/17.
 */
public class RoomTest {

    private Room testRoom = new Room("testRoom");

    @Test
    public void shortDescriptionTest(){

        String expectedDescription = "testRoom";
        assertEquals(expectedDescription, testRoom.shortDescription());
    }

    @Test
    public void addItemTest(){
        Item testItem = new Item("testItem","Available","Test","", 0,0);
        assertTrue(testRoom.addItem(testItem));
    }

    @Test
    public void getObjectiveTest(){
        testRoom.setObjective("Kill");
        String expectedObjective = "Kill";
        assertEquals(expectedObjective, testRoom.getObjective());
    }

    @Test
    public void getObjectiveItemTest(){
        testRoom.setObjectiveItem("Key");
        String expectedObjectiveItem = "Key";
        assertEquals(expectedObjectiveItem, testRoom.getObjectiveItem());
    }

}
