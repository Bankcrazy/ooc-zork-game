Commands:
 -'player-info':
    Print current player's information including health and inventory

 -'map-info':
    Print current map's information including room description, exits, items, monster

 -'take {item-name}':
    Take specify item from the room that player are in and added that item into player's inventory
    If the item already exists in player's inventory, it will add extra durability into that item instead

 -'drop {item-name}':
    Remove specify item from player's inventory, once item are removed it is gone forever

 -'use {item-name}':
    Apply the specify item to player, item must be of type 'Consumable' in order for player to be able to use

 -'go {direction}':
    Player move toward one of specific four direction (north,south,west,east) and change the current room that player are in

 -'attack-with {item-name}':
    Player perform attack to monster in the room with specify item, item must be of type 'Weapon' in order for player to attack

 -'help':
    Print all the available commands

 -'quit':
    Terminate the program, and quit the game



Rules:
	
	Player:
	    1. Player can use all the available commands listed above to perform various actions
	    2. Player has maximum health of 100
	    3. Player has maximum inventory slot of 3 to keep various items
	    4. Player will die if health are equal or below 0 and program/game are terminated

	Monster:
		1. Monster does not attack unless provoked by player 'attack-with {item-name}' command
		2. Monster has various health value from 10-100
		3. Monster has various hit points or damage value from 5-50
		4. Monster has one of the two status 'Alive' or 'Dead' shown in parenthesis after monster-name in a room
		       If 'Alive' player can attack the monster
		       If 'Dead' player can no longer perform attack

	Items:
		1. Item can be interact through following commands ['take','use','attack-with','drop']
		2. Item has durability that decrease by 1 for each usage from 'use' or 'attack-with' command
		3. Item has various durability value from 1 to 3
		4. Item has various damage value from 5 to 50
		5. Item has one of three type: 'Weapon', 'Consumable', or Objective'
		    All type can be interact with 'take' command
		    'Weapon' and 'Consumable' can be interact with 'drop' command

		    'Weapon' type:
		        - can be interact with 'attack-with {item-name}' command
		        - has 2 sub-type variation: 'Melee' and 'Range'
		        - with 'Melee' type monster can retaliate and attack player back
		        - with 'Range' type monster cannot retaliate and attack player
		        - 'Range' type weapon are ideal for fighting scenario, since player can deal damage from far away
		           safe from monster retaliation and attack

		    'Consumable' type:
		        - can be interact with 'use {item-name}' command
		        - all consumable will result in positive effect of restoring player health with different amount
		          according to each item damage value
		        - for consumable type damage value will act as an healing value instead of physical damage aspect

		    'Objective type':
		        - objective item once taken by player, cannot be drop because it is required to access certain room,
		          and in some case, required to clear certain level in the game
		        - objective item are required in order for player to proceed out of the current room

        6. Item has one of the two status 'Available' or 'Taken' shown in parenthesis after item-name in a room
                'Available' item can be pick up by 'take{item-name}' command
                'Taken' item can not be interact through any commands, it use to only show the player
                        that this item used to exist in this room

    Rooms:
        1. Room will have a description
        2. Room may or may not have items in it, maximum of 3 items per room
        3. Room may or may not have monster in it, maximum of 1 monster per room
        4. Room may or may not have objective in it, maximum 1 objective per romm
                There are two type of objective 'Kill' and 'Item'
                For 'Kill' objective: player must eliminate the monster in the room to proceed further
                For 'Item' objective player must obtain specific item to proceed further