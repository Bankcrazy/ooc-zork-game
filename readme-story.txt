Zork Game Story:

    The world has collapse and chaos spread all over. People in this world have been infected with the dangerous
   disease that turn them into the living dead or zombies. You are one of the few survivor of this apocalyptic
   event. In order to survive and regain hope for humanity, you must travel to the world global health center
   for a daring and life threatening mission. You must enter this health center and retrieve the cure for the
   disease.

Objective:
    There are 3 levels in this game, each level contains a specific objective in order to proceed to the next level
    Each levels contains many rooms with various monster and items for player to explore

    Level 1:
        in order to complete this level,
            player must navigate through the map and find staircase room to level 2
            player also need to defeat the mini-boss zombie guarding the staircase

    Level 2:
        in order to complete this level,
            player must navigate through the map and find virus-sample-laboratory room to proceed to level 3
            player also need to find the key to access this laboratory

    Level 3:
        in order to complete this level,
            player must navigate through the map and find a room that has the cure for zombies disease
            player also need to defeat the final boss that is guarding the cure

