Things to ignore:

1. target/
    Target folder is a result of source code compilation which is not necessary.

2. *.iml
	All iml files are made by IntelliJ and irrelevant to the project functionality.

3. .idea/
	Idea folder are made by IntelliJ and irrelevant to the project functionality.

4. .DS_Store
    All Ds_Store file are made up by Mac OS and contain custom attributes of its containing folder, which is
    irreverent to the project