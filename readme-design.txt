This design is to divide the task into 10 java classes with each class doing the following:

    ZorkGameRunner:
        This class act as a main java class to run the program by creating a new ZorkGame object and
        call upon it .play() method to execute the Zork game.

    ZorkGame:
        This class is where all the heavy lifting work take place. It combine and use all of the class in this
        game except for ZorkGameRunner class. The class main purpose is to dictate what each command the player or
        user issue and process the command into different actions by using  processCommand(Command) method.
        The command that get send from Commad class will get process and classify in determination
        whether or not it is valid command. If commands are valid it get through the function and perform
        the actions accordingly to the command. All of the command interactions and rules specify in readme-instruction is
        implement in this class. It also act as an level creation by adding various room. With each room that
        may or may not contain the following items, monster, and objective. By using this design everything is centralized
        and all the record are always up to date with each command perform.

    CommandParser:
        The raw text input from the commandline will get process through this class and send over to Command class,
        for processing the text.

    Command:
        This class identify all the valid command text that can be recognizable by the game and contain different methods
        to help ZorkGame class classify the text further.

    Room:
        This class act as an object prototype that contains all the necessary fields and methods to create a new room.
        It contains all the description, items, monster, and objective fields that can be use and access through many
        methods. Therefore making each room can be unique and provide fun experience for the player. The room specifications
        are following: maximum three item per room, maximum one monster per room, and only one objective per room. These feild
        can be empty but they cannot exceed the specify criteria. However, description field is required to make a room.

    Player:
        This class keep all the information about the player for example health and items. It also contains method for querying
        and modifying these data.

    MonsterFactory:
        This class keep all of the monsters data in the game, and contain a method to produce and make a new Monster that
        replicate the same data as specify in the MonsterFactory.

    Monster:
        This class act as an monster prototype that contains all the necessary fields and methods to create a new monster.
        It contains many fields that are necessary information in order to make Monster. Also provide useful methods in querying
        and modifying those fields.

    ItemFactory:
        This class keep all of the items data in the game, and contain a method to produce and make a new Item that
        replicate the same data as specify in the ItemFactory.

    Item:
        This class act as an item prototype that contains all the necessary fields and methods to create a new item.
        It contains many fields that are necessary information in order to make Item. Also provide useful methods in querying
        and modifying those fields.

For More details about the rules, interaction with the game, and in-depth game mechanics:
    Please read: readme-instruction.txt